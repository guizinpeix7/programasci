<head>
    <link rel="stylesheet" href="<?=base_url("assets/css/ToPractice.css");?>">
</head>
<body>
    <br>
    <h1 class="text-center"> Prática de CSS </h1>
    <hr>
    <div class="container">
        <div class="box-1">
            <h2> Border and fonts </h2>
            <p>
                Text box created to hone the knowledges of border, box, fonts and text alignment.
            </p>
        </div>
        
        <div class="box-2">
            <h2 class="text-center"> About incomprehensible things </h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
        </div>

        <div class="categories">
            <h2 class="text-center"> Categories </h2>
                <ul>
                    <li><a href="#"> Category 1 </a></li>
                    <li><a href="#"> Category 2 </a></li>
                    <li><a href="#"> Category 3 </a></li>
                    <li><a href="#"> Category 4 </a></li>
                </ul>
        </div>  
        
        <form class="my-form">
            <div class="form-group">
                <label>Name: </label>
                <input type="text" name="email">
            </div>
            <div class="form-group">
                <label>Email: </label>
                <input type="text" name="email">
            </div>
            <div class="form-group">
                <label>Content: </label>
                <textarea type="textarea" name="email">
            </textarea>
            <div class="text-center">
                <input type="submit" class="button">
            </div>
        </form>

        <br> <br>

        <div class="block"> <!--  horizontal alignment-->
            <p>
                Text boxes used to learn and hone knowedgments about horizontal layout, always remember to implement the clr class to correct the box content.
            </p>
        </div>

        <div class="block">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
            </p>
        </div>

        <div class="block" id="third-block">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
            </p>
        </div>
        
        <div class="clr">

        <br>
        <!-- Position -->
        
        <div class="p-box">
            <h1> PositionA </h1>
            <h2> PositionB </h2>
        </div>
    </div> <!-- Container ends -->
    <div class="fix">
        <div class="button">press me!</div>
        <!-- <div class="button"> No press me!</div> -->
    </div>
    
</body>

