<link rel="stylesheet" href="<?=base_url("assets/css/ToPractice.css");?>">
<body>
<br>
<h1 class="text-center"> <?php echo $title;?> </h1>
    <hr>

    <div class="container">
    <ul class="text-center categories">
        <li><a href="<?=base_url()?>PHP/Testsphp"> default controller exemple </a> <p>(This page is default)<p> </li>    
        <li><a href="<?=base_url()?>PHP/Testsphp/comments"> Simple controller call exemple </a></li>     
        <li><a href="<?=base_url()?>PHP/Testsphp/paramsT/exemple/123"> Controller with params method exemple </a></li>     
        <li><a href="<?=base_url()?>PHP/Testsphp/_hidenmethod"> Hidden method exemple </a></li>     
        <li><a href="<?=base_url()?>PHP/Testsphp/_hidenmethod2"> Redirecting one route to another </a></li>     
        
        <br>
        <p class="text-left">Note: Everything in this page is redirected by _remap() function. </p>
    </ul>
    <br>
    
</div>
</body>