<!DOCTYPE html>
<head> 
    <!-- site wrapping -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Boostrap 4.3.1 -->
    <link href="<?=base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
    <!-- Boostrap v1.1 -->
    <link href="<?=base_url("assets/css/bootstrap-extend.css");?>" rel="stylesheet">
    <!-- Extra CSS components -->
    <link rel="stylesheet" href="<?=base_url("assets/css/horizontal_menu_style.css");?>">
    <!-- Fonts awsome -->
    <link rel="stylesheet" href="<?=base_url("assets/css/all.css");?>">
    <link rel="stylesheet" href="<?=base_url("assets/css/font-awesome.min.css");?>">


    <!--Calculadora simples feita para por em pratica o aprendido-->
    
</head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <a class="navbar-brand" href="#">Aprendizado</a>

        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
            <a class="nav-item nav-link active" href="<?php echo base_url();?>">Home <span class="sr-only">(current)</span></a>
            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PHP</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="<?php echo base_url('PHP/matematica/calculadora');?>">Calculadora</a>
                <a class="dropdown-item" href="<?php echo base_url('PHP/TestsPHP/index');?>">Controllers</a>
        
                
                <!--<a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
                </div> --> 
            </li>
            <a class="nav-item nav-link" href="#">HTML</a>
            <a class="nav-item nav-link" href="<?php echo base_url('CSS/LearningDesign/Practice')?>">CSS</a>
            <a class="nav-item nav-link" href="#">JS</a>
            
            </div>
        </div>
    </nav>

    
    <?php //echo base_url(); ?>