<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mainpage extends CI_Controller {
    public function index(){
        
        $data = array(); 
        $page = 'home';
        $data['title'] = ucfirst($page);

        $this->load->view('include/header');
        $this->load->view($page, $data); 
        $this->load->view('include/footer');

        //Aqui o $page da o nome do arquivo a ser aberto 
        //$data sao os dados desta classe que serao compartilhadas com a view
    }
}