<?php
//Página que sera usada para desengessar a producao e compreenção de HTML + CSS
defined('BASEPATH') OR exit('No direct script access allowed');
class LearningDesign extends CI_Controller {
    public function Practice(){
        
        $data = array(); 
        $page = 'practice';
        $data['title'] = ucfirst($page);
        
        $this->load->view('include/header');
        $this->load->view('CSS/'.$page,$data); 
        $this->load->view('include/footer');

    }
}